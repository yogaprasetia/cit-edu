package com.cit.edu;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Home_Page extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private TabItem tab1Artikel, tab2Video, tab3Chat;
    public PageAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_page_layout);

        tabLayout = (TabLayout)findViewById(R.id.tab_menu);
        tab1Artikel = (TabItem)findViewById(R.id.tab1);
        tab2Video = (TabItem)findViewById(R.id.tab2);
        tab3Chat = (TabItem)findViewById(R.id.tab3);
        viewPager = (ViewPager)findViewById(R.id.viewpager);

        pagerAdapter = new PageAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(pagerAdapter);

        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.toolbar_menu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                Intent login = new Intent(Home_Page.this, Login_Page.class);
                switch (menuItem.getItemId()){
                    case R.id.item3:
                        Intent i = new Intent(Home_Page.this, Halaman_Admin.class);
                        if (user == null){
                            startActivity(login);
                        } else {
                            startActivity(i);
                        }
                        break;
                    case R.id.item2:
                        Intent i2 = new Intent(Home_Page.this, AkunSetting.class);
                        if (user == null){
                            startActivity(login);
                        } else {
                            startActivity(i2);
                        }
                        break;
                    case R.id.item1:
                        logout();
                        break;
                }
                return false;
            }
        });

        

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                if (tab.getPosition() == 0){
                    pagerAdapter.notifyDataSetChanged();
                }else if (tab.getPosition() == 1){
                    pagerAdapter.notifyDataSetChanged();
                }else if (tab.getPosition() == 2){
                    pagerAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }

    private void logout() {
        mAuth.signOut();
        Intent out = new Intent(Home_Page.this, Login_Page.class);
        startActivity(out);
        finish();
    }

}
